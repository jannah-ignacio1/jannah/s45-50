import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext';

import {Fragment} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router} from 'react-router-dom';
import {Routes, Route} from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
//import Banner from './components/Banner';
//import Highlights from './components/Highlights';
import Home from './pages/Home';
import Courses from './pages/Courses';
import CourseView from './pages/CourseView';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import ErrorPage from './pages/ErrorPage';
import './App.css';

function App() {

  const [user, setUser] = useState({
    //email: localStorage.getItem('email')
    id: null,
    password: null
  })


  //Function to clear the localStorage for logout
  const unsetUser = () => {
    localStorage.clear();
  }


  useEffect(() => {
    //console.log(user)
    //console.log(localStorage)
  
    fetch('http://localhost:4000/users/details', {
      headers: {
        Authorization:`Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      if(typeof data._id !== "undefined") {
        setUser({
          id: data.id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, [])


  return (
    /*<AppNavbar />,
    <Banner />*/
    //parent component --> {Fragment} NEED
    //Container for additional padding
    /*<Fragment> 
      <AppNavbar />
      <Container>
        <Banner />
        <Highlights />
      </Container>
    </Fragment>*/
   <UserProvider value = {{user, setUser, unsetUser}}> 
    <Router>
      <AppNavbar />
      <Container>
        {/*<Home />
        <Courses />
      <Register />
      <Login />*/}
        <Routes>
          <Route exact path="/" element={<Home/>} />
          <Route exact path="/courses" element={<Courses/>} />
          <Route exact path="/courses/:courseId" element={<CourseView/>} />
          <Route exact path="/login" element={<Login/>} />
          <Route exact path="/logout" element={<Logout/>} />
          <Route exact path="/register" element={<Register/>} />
          <Route exact path="*" element={<ErrorPage/>} />
        </Routes>
      </Container>
    </Router>
  </UserProvider>
  );
}

export default App;



