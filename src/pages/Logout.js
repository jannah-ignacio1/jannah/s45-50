import { Navigate } from 'react-router-dom';
import { useContext, useEffect } from 'react';
import UserContext from '../UserContext';

export default function Logout (){

	const {unsetUser, setUser} = useContext(UserContext);

	//this method will help/allow us to clear the information in the localStorage
	//localStorage.clear()

	unsetUser();

	 //add a useEffect to run our setUser. This useEffect will have an empty dependency array.
	useEffect(() => {
		setUser({
			//: null,
			//isAdmin: null,
			id: null
		})
	}, [])

	return (

		<Navigate to="/login" />
		
		)
}