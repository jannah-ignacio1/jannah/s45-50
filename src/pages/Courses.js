import PropTypes from 'prop-types';
import { Fragment, useEffect, useState } from 'react';
import CourseCard from '../components/CourseCard';
//import coursesData from '../data/coursesData';

export default function Courses() {

	//console.log(coursesData)

	//const courses = coursesData.map(course => {

		//return (
			//<CourseCard key={course.id} courseProp={course} />
	//)
	//})
	//State that will be used to store the courses retrieved from the database
	const [courses, setCourses] = useState([])

	useEffect(() => {
		fetch('http://localhost:4000/courses')
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setCourses(data.map(course => {
				return(
					<CourseCard key={course._id} courseProp={course} />
					)
			}))


		})
	}, [])

	return (

			<Fragment>
				{courses}
			</Fragment>
		)

}


CourseCard.propTypes = {

	courseProp: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}
//optional