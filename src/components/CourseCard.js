//import {useState, useEffect}from 'react';
import { Link } from 'react-router-dom';
import {Card, Button} from 'react-bootstrap';


export default function CourseCard({courseProp}){

	//console.log(props)
	//console.log(typeof props)
	//console.log(courseProp)
	const { name, description, price, _id } = courseProp 


	/*
		Syntax:
			const [getter, setter] - useState(initialGetterValue)
	*/
	//const [count, setCount] = useState(0)
	//const [seat, setSeat] = useState(30)

	/*function enroll () {
		if(count < 30) {
		//OR if(seat > 0)
		setCount(count + 1)
		setSeat(seat - 1)
		console.log('Enrollees:' + count)
		console.log('Seats:' + seat)
		} else {
			alert('No more seats.')
		}
	}*/

	//function enroll () {
		//setCount(count + 1);
		//console.log('Enrollees:' + count)
		//setSeat(seat - 1);
		//console.log('Seats:' + seat)s
	//}

	//useEffect(() => {
		//if (seat === 0) {
			//alert('No more seats.')
		//}
	//}, [seat])


	return(
		<Card>
		  <Card.Body>
		    <Card.Title>{name}</Card.Title>
		       <Card.Subtitle>Description:</Card.Subtitle>
		       <Card.Text>{description}</Card.Text>
		       <Card.Subtitle>Price:</Card.Subtitle>
		       <Card.Text>Php {price}</Card.Text>
		       {/*<Card.Text>Enrollees: {count}</Card.Text>
		       <Card.Text>Seats Available: {seat}</Card.Text>
		       <Button variant="primary" onClick={enroll}>Enroll</Button>*/}
		       <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
		  </Card.Body>
		</Card>
		)

}

