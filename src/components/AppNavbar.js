import { useState, Fragment, useContext } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar(){

  //const [user, setUser] = useState(localStorage.getItem("email"));
  const {user} = useContext(UserContext)
  console.log(user);


  return (
      <Navbar bg="light" expand="lg">

          <Navbar.Brand className="navbar-brand fw-bold fs-2 px-5 py-2" as={Link} to="/">Zuitt</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ml-auto mb-2 mb-lg-0">
              <Nav.Link as={ Link } to="/">Home</Nav.Link>
              <Nav.Link as={ Link } to="/courses">Courses</Nav.Link>
              { 
                (user.id !== null) ?

                <Nav.Link as={ Link } to="/logout">Logout</Nav.Link>
                :
                <Fragment>
                  <Nav.Link as={ Link } to="/login">Login</Nav.Link>

                  <Nav.Link as={ Link } to="/register">Register</Nav.Link>
                </Fragment>
              }
              
            </Nav>
          </Navbar.Collapse>
      </Navbar>
    )
}



/*
Activity sol'n

import { useState, Fragment} from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link } from 'react-router-dom';

export default function AppNavbar(){

  const [user, setUser] = useState(localStorage.getItem("email"));
  console.log(user);



  return (
    <Navbar bg="light" expand="lg">
        <Navbar.Brand as={ Link } to="/">Zuitt</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto">
            <Nav.Link as={ Link } to="/">Home</Nav.Link>
            <Nav.Link as={ Link } to="/courses">Courses</Nav.Link>

            {
              (user !== null ) ?

              <Nav.Link as={ Link } to="/logout">Logout</Nav.Link>

              :
              
              <Fragment>
                <Nav.Link as={ Link } to="/login">Login</Nav.Link>
                <Nav.Link as={ Link } to="/register">Register</Nav.Link>
              </Fragment>
            }

          </Nav>
        </Navbar.Collapse>
    </Navbar>
  )
}*/

/*on BANNER page: 

// import Button from 'react-bootstrap/Button';
// import Row from 'react-bootstrap/Row';
// import Col from 'react-bootstrap/Col';
import {Button, Row, Col} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({data}) {

  console.log(data)

  const { title, content, destination, label} = data

  return(

    <Row>
      <Col className="p-5">
        <h1>{title}</h1>
        <p>{content}</p>
        <Button variant="primary" as={Link} to={destination}>{label}</Button>
      </Col>
    </Row>
    )
}
*/


/*
on Home Page
import {Fragment} from 'react';
import Banner from '../components/Banner';
//import CourseCard from '../components/CourseCard';
import Highlights from '../components/Highlights';


export default function Home () {

  const data = {
    title: "Zuitt Coding Bootcamp",
    content: "Opportunities for everyone, everywhere",
    destination: "/courses",
    label: "Enroll Now!"

  }

  return(
    <Fragment>
      <Banner data={data}/>
      <Highlights/>
        </Fragment>
    )


}


on Error page
import Banner from '../components/Banner';

export default function Error() {

  const data = {
    title: "404 - Not Found",
    content: "The page you are looking for cannot be found",
    destination: "/",
    label: "Back Home"
  }

  return(
      <Banner data={data} />
    )

}


*/