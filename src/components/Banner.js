//import Button from 'react-bootstrap/Button';
//import Row from 'react-bootstrap/Row';
//import Col from 'react-bootstrap/Col';

//convenient way
import { Button, Row, Col } from 'react-bootstrap';
import { Fragment } from 'react';
import { useLocation, Link } from 'react-router-dom';

//useLocation() hook returns the location object that represents the current URL. You can think about it like a useState that returns a new location whenever the URL changes.

/*export default function Banner(){
	


	return(
		
		<Row>
			<Col className="p-5">
				<h1>Zuitt Coding Bootcamp</h1>
				<p>Opportunities for everyone, everywhere.</p>
				<Button variant="primary">Enroll Now!</Button>
			</Col>
		</Row>


		)
}

/*Activity*/
export default function Banner(){

	const url=useLocation()
	const path=url.pathname
//example: const pathname = window.location.pathname --> returns the current url minus the domain name

	return(

		<Fragment>{
		(path === '/') ?
		<Row>
			<Col className="p-5">
				<h1>Zuitt Coding Bootcamp</h1>
				<p>Opportunities for everyone, everywhere.</p>
				<Button variant="primary">Enroll Now!</Button>
			</Col>
		</Row>
		:
		<Fragment>
		<Row>

			<Col className="p-5">
				<h1>Page Not Found</h1>
				<p>Go back to the <Link as={ Link } to="/">homepage.</Link></p>
			</Col>
		</Row>
		</Fragment>
		}
		</Fragment>
		)
}
