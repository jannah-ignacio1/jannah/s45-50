import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
//import AppNavbar from './AppNavbar';
//import bootstrap CSS from https://react-bootstrap.github.io/getting-started/introduction/
//import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';
//import 'font-awesome/css/font-awesome.min.css';
  //to install: npm i font-awesome

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);


//const name = 'John Smith';
//const element = <h1> Hello, {name} </h1>
//output: Hello, John Smith

/*
const user = {
  firstName: "Jane",
  lastName: "Smith"
}

function formatName(user) {
  return user.firstName + ' ' + user.lastName
}

const element = <h1> Hello, {formatName(user)} </h1>
//output: Hello, Jane Smith


//rendering using JSX element
ReactDOM.render(
  element, 
  document.getElementById('root')
  );

*/